package com.apirest.producto.model;

import com.apirest.producto.model.common.Generic;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.ArrayList;
import java.util.List;

@Document(collection="productos")
public class Producto extends Generic {
    public String descripcion;
    public double precio;
    public List<Proveedor> idsProveedores = new ArrayList<>();

    public Producto() {
    }

    public Producto(String id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }
}
