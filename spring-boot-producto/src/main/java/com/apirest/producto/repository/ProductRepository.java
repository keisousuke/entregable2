package com.apirest.producto.repository;

import com.apirest.producto.model.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<Producto, String> {
}
