package com.apirest.producto.service.impl;

import com.apirest.producto.model.Producto;
import com.apirest.producto.repository.ProductRepository;
import com.apirest.producto.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Producto> getProductos() {
        return productRepository.findAll();
    }

    @Override
    public Producto getProducto(String index){
        Optional<Producto> optionalProducto = productRepository.findById(index);
        return optionalProducto.isPresent() ? optionalProducto.get(): null;
    }

    @Override
    public Producto addProducto(Producto newPro) {
        return productRepository.insert(newPro);
    }

    @Override
    public Producto updateProducto(String index, Producto newPro){
        newPro.id = index;
        return productRepository.save(newPro);
    }

    @Override
    public void removeProducto(String index){
        productRepository.deleteById(index);
    }
}
