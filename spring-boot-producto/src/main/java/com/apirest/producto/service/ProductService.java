package com.apirest.producto.service;

import com.apirest.producto.model.Producto;

import java.util.List;

public interface ProductService {
    List<Producto> getProductos();
    Producto getProducto(String index);
    Producto addProducto(Producto newPro);
    Producto updateProducto(String index, Producto newPro);
    void removeProducto(String index);
}
