package com.apirest.producto.controller;

import com.apirest.producto.model.ProductoPrecio;
import com.apirest.producto.model.Producto;
import com.apirest.producto.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("${url.base}/productos")
public class ProductController {
    @Autowired
    ProductService productService;
    @GetMapping
    public List<Producto> getProductos() {
        List<Producto> productoList = productService.getProductos();
        if(Objects.isNull(productoList) || productoList.isEmpty())
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        return productService.getProductos();
    }
    @GetMapping("/{id}")
    public ResponseEntity getProductoId(@PathVariable String id) {
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }
    @PostMapping
    public ResponseEntity<String> addProducto(@RequestBody Producto producto) {
        productService.addProducto(producto);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    public ResponseEntity updateProducto(@PathVariable String id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteProducto(@PathVariable String id) {
        Producto pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PatchMapping("/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecio productoPrecioOnly, @PathVariable String id){
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.precio=productoPrecioOnly.precio;
        productService.updateProducto(id, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }
    @GetMapping("/{id}/proveedores")
    public ResponseEntity getProductIdUsers(@PathVariable String id){
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (Objects.nonNull(pr.idsProveedores) && !pr.idsProveedores.isEmpty())
            return ResponseEntity.ok(pr.idsProveedores);
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
